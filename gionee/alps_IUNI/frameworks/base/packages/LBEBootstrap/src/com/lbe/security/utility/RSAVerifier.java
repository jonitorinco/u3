package com.lbe.security.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigInteger;

public class RSAVerifier {
	private static String PUBLIC_N = "9699109ACD4DA6255D61E4340124FB67213264033E8CE63C26647CC2757794C9665EC9BFB01516C2FED8D4FE7F5B8CDD23D6D425F59525FC808112B504B63591C41DA6A8A97F14D7CCE5CDEAD268D0675DEF754011DFD28DB01F91E2328F21161328195AEDD5178589F6F88081A4E2FB1263D551DCF2FE22BFD49FEE0276D3C1";
	private static String PUBLIC_E = "3ECC3";
	
	private static BigInteger N = new BigInteger(PUBLIC_N, 16);
	private static BigInteger E = new BigInteger(PUBLIC_E, 16);
	
	private static byte[] _decrypt(String cipher) {
		BigInteger C = new BigInteger(cipher, 16);
		byte[] raw = C.modPow(E, N).toByteArray();
		byte[] plain = new byte[raw[raw.length - 1]];
		for (int i=raw.length - 2; i>= 0; i--) {
			plain[plain.length - i - 1] = raw[i];
		}
		
		return plain;
	}
	
	public static boolean validateRSAKey(String apiKey, int pid) {
		String processName = decrypt(apiKey);
		String cmdLine = null;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(String.format("/proc/%d/cmdline", pid))));
			cmdLine = reader.readLine().trim();
			if (cmdLine.indexOf(':') > 0) {
				cmdLine = cmdLine.substring(0, cmdLine.indexOf(':'));
			}
			reader.close();
		} catch (Exception e) {
		}
		
		return processName != null && processName.equals(cmdLine);
	}
	
	public static String decrypt(String cipher) {
		if (cipher != null) {
			return new String(_decrypt(cipher));
		} else {
			return null;
		}
	}
}
