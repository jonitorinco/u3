/*
******************************************************
*	深圳市福田区深南大道7888东海国际中心B座16层IUNI科技		
*	文 件 名：AuroraMetaMediaScanner.h
*	创建时间：2014-04-14
*	文件描述：多媒体扫描文件底层处理
*====================================================
*	维护人：hjw
*====================================================
* 	代码版本：1.0
*	修改时间：2014-04-14
*	修改人：hjw
*	修改描述：创建
*====================================================
*	遗留问题：
*
******************************************************
*/



#ifndef INUIMEDIASCANNER_H_

#define INUIMEDIASCANNER_H_

#include "AuroraMediaScanner.h"

namespace android {

struct AuroraMetaMediaScanner : public AuroraMediaScanner {
    AuroraMetaMediaScanner();
    virtual ~AuroraMetaMediaScanner();

    virtual MediaScanResult processFile(
            const char *path, const char *mimeType,
            AuroraMediaScannerClient &client);

    virtual char *extractAlbumArt(int fd);

private:
    AuroraMetaMediaScanner(const AuroraMetaMediaScanner &);
    AuroraMetaMediaScanner &operator=(const AuroraMetaMediaScanner &);

    MediaScanResult processFileInternal(
            const char *path, const char *mimeType,
            AuroraMediaScannerClient &client);
};

}  

#endif  // INUIMEDIASCANNER_H_

